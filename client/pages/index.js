import React, {Component} from "react"
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker,
  Annotation,
} from "react-simple-maps"
import chroma from "chroma-js"
import {scaleLinear} from "d3-scale"
import request from "axios"
import Header from './header'
import {setupWebSocket} from "./utils";

const wrapperStyles = {
  width: "100%",
  maxWidth: 1200,
  margin: "0 auto",
  marginTop: "-360px"
}

const cityScale = scaleLinear()
  .domain([0, 37843000])
  .range([1, 25])

const colorScale = chroma
  .scale([
    '#FF6E40',
    'FFD740',
    '#00B8D4',
  ])
  .mode('lch')
  .colors(24)

const subregions = [
  "Polynesia",
  "Micronesia",
  "Central Asia",
  "Melanesia",
  "Western Europe",
  "Seven seas (open ocean)",
  "Northern Africa",
  "Caribbean",
  "South-Eastern Asia",
  "Eastern Africa",
  "Australia and New Zealand",
  "Antarctica",
  "Eastern Europe",
  "Southern Europe",
  "Northern Europe",
  "Western Africa",
  "Middle Africa",
  "Southern Africa",
  "Southern Asia",
  "Eastern Asia",
  "Western Asia",
  "South America",
  "Central America",
  "Northern America",
]

const popScale = scaleLinear()
  .domain([0, 100000000, 1400000000])
  .range(["#CFD8DC", "#607D8B", "#37474F"])

class UpdatableChoropleth extends Component {
  constructor() {
    super()

    this.state = {
      populationData: false,
      cities: [],
      plague_name: 'PLG-007',
      continent: 'Asia',
      plague_X: 100,
      plague_Y: 40,
      plague_Z: 15,
      africa_infection: 0,
      america_infection: 0,
      europa_infection: 0,
      asia_infection: 0,
      cure: 0,
      dna: 0,
      severity: 0,
      lethality: 0,
      infectability: 0,
      date: new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear(),
    }

    this.switchToPopulation = this.switchToPopulation.bind(this)
    this.switchToRegions = this.switchToRegions.bind(this)
    this.fetchCities = this.fetchCities.bind(this)
    this.updatePlagueName = this.updatePlagueName.bind(this)
    this.updateContinent = this.updateContinent.bind(this)
    this.startGame = this.startGame.bind(this)
  }

  switchToPopulation() {

    this.setState({populationData: true})
  }

  switchToRegions() {
    this.setState({populationData: false})
  }

  componentDidMount() {
    this.fetchCities()
  }

  updatePlagueName(e) {
    this.setState({plague_name: e.target.value})
  }

  updateContinent(e) {
    this.setState({continent: e.target.value})
    switch (e.target.value) {
      case 'Asia':
        this.setState({plague_X: 100, plague_Y: 40})
        break
      case 'America':
        this.setState({plague_X: -100, plague_Y: 40})
        break
      case 'Africa':
        this.setState({plague_X: 10, plague_Y: 20})
        break
      case 'Europa':
        this.setState({plague_X: 10, plague_Y: 50})
        break
      case 'Oceania':
        this.setState({plague_X: 130, plague_Y: -20})
        break
    }
  }

  fetchCities() {
    request
      .get("/static/world-most-populous-cities.json")
      .then(res => {
        this.setState({
          cities: res.data,
        })
      })
  }

  startGame() {
    const self = this;
    const socket = setupWebSocket();
    this.socket = socket;
    this.socket.onopen = function () {
      socket.send(JSON.stringify({
        type: 'START_GAME',
        plague_name: self.state.plague_name,
        start_continent: self.state.continent
      }));
    };
    socket.onmessage = function (event) {
      console.info(`[message] Data received <- server`);
      const eventData = JSON.parse(event.data);
      console.log(eventData);
      if (eventData.type === 'DNA_INCREMENT') {
        self.setState({dna: eventData.DNA});
      }
    };
  }

  render() {
    return (
      <div>
        <Header/>
        <div className="row">
          <div className="col s2">
            <h5><b>Plaga:</b> {this.state.plague_name}</h5>
            <p><b>Continente:</b> {this.state.continent}</p>
            <p><b>Fecha:</b> {this.state.date}</p>
            <p><b>DNA:</b> {this.state.dna} XP</p>
            <p><b>Cura:</b> {this.state.cure}%</p>
            <div className="progress">
              <div className="determinate" style={{width: this.state.cure + '%'}}></div>
            </div>
            <p><b>Severidad:</b> {this.state.severity}%</p>
            <div className="progress">
              <div className="determinate" style={{width: this.state.severity + '%'}}></div>
            </div>
            <p><b>Letalidad:</b> {this.state.lethality}%</p>
            <div className="progress">
              <div className="determinate" style={{width: this.state.lethality + '%'}}></div>
            </div>
            <p><b>Infectabilidad:</b> {this.state.infectability}%</p>
            <div className="progress">
              <div className="determinate" style={{width: this.state.infectability + '%'}}></div>
            </div>
          </div>
        </div>
        <div className="fixed-action-btn">
          <a className="btn-floating btn-large red">
            Trans
          </a>
          <ul>
            <li><a className="btn-floating red">1</a></li>
            <li><a className="btn-floating yellow darken-1">2</a></li>
            <li><a className="btn-floating green">3</a></li>
            <li><a className="btn-floating blue">4</a></li>
          </ul>
        </div>
        <div className="fixed-action-btn" style={{right: "100px"}}>
          <a className="btn-floating btn-large blue">
            Skills
          </a>
          <ul>
            <li><a className="btn-floating blue">1</a></li>
            <li><a className="btn-floating yellow darken-1">2</a></li>
            <li><a className="btn-floating green">3</a></li>
            <li><a className="btn-floating red">4</a></li>
          </ul>
        </div>
        <div id="Welcome" className="modal">
          <div className="modal-content">
            <h4>Plague Inc</h4>
            <div className="row">
              <form className="col s12">
                <div className="row">
                  <div className="input-field col s6">
                    <input placeholder="PLG-007" id="plague_name" type="text" className="validate"
                           onChange={this.updatePlagueName}/>
                    <label for="plague_name">Nombre de la plaga</label>
                  </div>
                  <div class="input-field col s6">
                    <select id="continent" onChange={this.updateContinent} defaultValue="Asia">
                      <option value="Asia">Asia</option>
                      <option value="America">America</option>
                      <option value="Africa">Africa</option>
                      <option value="Europa">Europa</option>
                      <option value="Oceania">Oceania</option>
                    </select>
                    <label>Seleccione continente</label>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div className="modal-footer">
            <button className="modal-close waves-effect waves-green btn-flat" onClick={this.startGame}>Empezar</button>
          </div>
        </div>
        <div style={wrapperStyles}>
          <ComposableMap
            projectionConfig={{
              scale: 205,
              rotation: [-11, 0, 0],
            }}
            width={980}
            height={551}
            style={{
              width: "100%",
              height: "auto",
            }}
          >
            <ZoomableGroup center={[0, 20]}>
              <Geographies
                geography={"/static/world-50m-with-population.json"}
                disableOptimization
              >
                {(geographies, projection) =>
                  geographies.map((geography, i) => (
                    <Geography
                      key={`${geography.properties.iso_a3}-${i}`}
                      cacheId={`${geography.properties.iso_a3}-${i}`}
                      geography={geography}
                      projection={projection}
                      onClick={this.handleClick}
                      round
                      style={{
                        default: {
                          fill: this.state.populationData
                            ? popScale(geography.properties.pop_est)
                            : colorScale[subregions.indexOf(geography.properties.subregion)],
                          stroke: "#607D8B",
                          strokeWidth: 0.75,
                          outline: "none",
                        },
                        hover: {
                          fill: this.state.populationData
                            ? "#263238"
                            : chroma(colorScale[subregions.indexOf(geography.properties.subregion)]).darken(0.5),
                          stroke: "#607D8B",
                          strokeWidth: 0.75,
                          outline: "none",
                        },
                        pressed: {
                          fill: this.state.populationData
                            ? "#263238"
                            : chroma(colorScale[subregions.indexOf(geography.properties.subregion)]).brighten(0.5),
                          stroke: "#607D8B",
                          strokeWidth: 0.75,
                          outline: "none",
                        }
                      }}
                    />
                  ))}
              </Geographies>
              {/* TRANSPORT */}
              <Markers>
                {/* AFRICA */}
                <Marker marker={{coordinates: [5, 47.3]}}>
                  <circle cx={0} cy={150} r={5}/>
                </Marker>
                {/* AMERICA */}
                <Marker marker={{coordinates: [-65, 80]}}>
                  <circle cx={0} cy={150} r={5}/>
                </Marker>
                {/* EUROPA */}
                <Marker marker={{coordinates: [0, 90]}}>
                  <circle cx={0} cy={150} r={5}/>
                </Marker>
                {/* ASIA */}
                <Marker marker={{coordinates: [100, 0]}}>
                  <circle cx={0} cy={10} r={5}/>
                </Marker>
              </Markers>
              {/* CONTINENTS PERCENT */}
              <Markers>
                {/* AFRICA */}
                <Marker marker={{coordinates: [10, 20]}}>
                  <circle cx={0} cy={10} r={5} fill="rgba(255,87,34,0.8)" stroke="#607D8B" strokeWidth="2"/>
                </Marker>
                {/* AMERICA */}
                <Marker marker={{coordinates: [-100, 40]}}>
                  <circle cx={0} cy={10} r={5} fill="rgba(255,87,34,0.8)" stroke="#607D8B" strokeWidth="2"/>
                </Marker>
                {/* EUROPA */}
                <Marker marker={{coordinates: [10, 50]}}>
                  <circle cx={0} cy={10} r={5} fill="rgba(255,87,34,0.8)" stroke="#607D8B" strokeWidth="2"/>
                </Marker>
                {/* ASIA */}
                <Marker marker={{coordinates: [100, 40]}}>
                  <circle cx={0} cy={10} r={5} fill="rgba(255,87,34,0.8)" stroke="#607D8B" strokeWidth="2"/>
                </Marker>
                {/* CURRENT PLAGUE */}
                <Marker marker={{coordinates: [this.state.plague_X, this.state.plague_Y]}}>
                  <circle cx={0} cy={10} r={this.state.plague_Z} fill="rgba(255,87,34,0.8)" stroke="#607D8B"
                          strokeWidth="2"/>
                </Marker>
              </Markers>
              {/* AFRICA TEXT */}
              <Annotation
                dx={10}
                dy={-30}
                subject={[10, 18]}
                strokeWidth={2}
                stroke="#607D8B"
              >
                <text>{this.state.africa_infection}%</text>
              </Annotation>
              {/* AMERICA TEXT */}
              <Annotation
                dx={10}
                dy={-30}
                subject={[-100, 38]}
                strokeWidth={2}
                stroke="#607D8B"
              >
                <text>{this.state.america_infection}%</text>
              </Annotation>
              {/* EUROPA TEXT */}
              <Annotation
                dx={10}
                dy={-30}
                subject={[10, 48]}
                strokeWidth={2}
                stroke="#607D8B"
              >
                <text>{this.state.europa_infection}%</text>
              </Annotation>
              {/* ASIA TEXT */}
              <Annotation
                dx={10}
                dy={-30}
                subject={[100, 38]}
                strokeWidth={2}
                stroke="#607D8B"
              >
                <text>{this.state.asia_infection}%</text>
              </Annotation>
            </ZoomableGroup>
          </ComposableMap>
        </div>
      </div>
    )
  }
}

export default UpdatableChoropleth
