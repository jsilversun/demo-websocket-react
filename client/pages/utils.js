export function setupWebSocket(host = 'localhost', port = 1337) {
    const socket = new WebSocket(`ws://${host}:${port}`);

    socket.onopen = function (e) {
        console.info('[open] Connection established');
    };

    socket.onclose = function (event) {
        if (event.wasClean) {
            console.info(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        } else {
            console.error('[close] Connection died');
        }
    };

    socket.onerror = function (error) {
        console.error(`[error] ${error.message}`);
    };
    return socket;
}
