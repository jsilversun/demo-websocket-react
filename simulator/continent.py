

class Continent:
    name = ""
    population = 0
    deaths = 0
    infected = 0
    is_wealthy = False
    is_urban = False
    ships = []
    planes = []

    def __init__(self, name, is_wealthy, is_urban, population):
        self.name = name
        self.is_wealthy = is_wealthy
        self.is_urban = is_urban
        self.population = population

    def __str__(self):
        return '{name} - {population}'.format(name=self.name, population=self.population)

    @property
    def healthy(self):
        healthy = self.population - self.infected - self.deaths
        return healthy if healthy > 0 else 0

    def apply_plague_impact(self):
        self.infect_population()
        self.kill_population()

    def infect_population(self):
        future_infected = self.infected * 2
        if future_infected > self.healthy:
            future_infected = self.infected + self.healthy
        self.infected = future_infected

    def kill_population(self):
        future_deaths = self.deaths + int(self.infected / 8)
        if self.population < future_deaths:
            future_deaths = self.population
        if future_deaths == self.deaths and self.healthy == 0:
            future_deaths = self.deaths + self.infected
        self.infected -= future_deaths - self.deaths
        self.deaths = future_deaths
