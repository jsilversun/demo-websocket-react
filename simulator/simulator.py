import asyncio
import json
import logging
import websockets
import random
import functools
from continent import Continent
from travel import Travel
from plague import Plague
from data import continents as continents_data
from data import travels as travels_data


class PIEvent:
    generate_event = None
    data = {}
    type = None

    def __init__(self, **kwargs):
        expected_args = ['type', 'data', 'duration', 'generate_event']
        for arg in expected_args:
            value = kwargs.get(arg)
            if value:
                setattr(self, arg, value)


state = {
    'DNA': 0,
    'continents': [],
    'travel': {'plane': [], 'ship': []},
    'plague': None
}
parameters = {
    'time_speed': 1
}
fel = []


def generate_event(data):
    fel.append({'duration': random.randint(1, 4), **data})


def add_event_to_fel(data):
    fel.append(data)


def load_continents():
    for continent_data in continents_data:
        state['continents'].append(Continent(**continent_data))


def load_travels():
    for travel_data in travels_data:
        continent1 = [_ for _ in state['continents'] if _.name == travel_data['continent1']]
        continent2 = [_ for _ in state['continents'] if _.name == travel_data['continent2']]
        if len(continent1) == 0:
            raise Exception('Continent {continent1} not found'.format(continent1=travel_data['continent1']))
        if len(continent2) == 0:
            raise Exception('Continent {continent2} not found'.format(continent2=travel_data['continent2']))
        travel = Travel(travel_data['type'], continent1[0], continent2[0], travel_data['frequency'])
        if getattr(travel, 'type') == 'plane':
            state['travel']['plane'].append(travel)
            continent1[0].planes.append(travel)
            continent2[0].planes.append(travel)
        else:
            state['travel']['ship'].append(travel)
            continent1[0].ships.append(travel)
            continent2[0].ships.append(travel)


def generate_travel(event_type, travel_type):
    options = ['continent1', 'continent2']
    # 1. Select a random sailing travel
    travel_index = random.randint(0, len(state['travel'][travel_type]) - 1)
    travel = state['travel'][travel_type][travel_index]
    # 2. Generate random start/end for the flight/sailing
    start = random.randint(0, 1)
    start_index = options.pop(start)
    end_index = options.pop()
    start_continent = getattr(travel, start_index)
    end_continent = getattr(travel, end_index)
    is_infected = True if random.randint(0, 1) == 1 else False
    # 3. Format event
    data = {
        'from': start_continent.name,
        'to': end_continent.name,
        'isInfected': is_infected,
    }
    start_event_data = {
        'type': '{type}_START'.format(type=event_type),
        **data,
    }
    end_event_data = {
        'type': '{type}_END'.format(type=event_type),
        **data,
    }
    # 4. Add event to FEL
    generate_next_event = generate_ship_travel if event_type == 'ship' else generate_plane_travel
    start_event = PIEvent(
        type=event_type, data=start_event_data, duration=random.randint(1, 5), generate_event=generate_next_event)
    end_event = PIEvent(
        type=event_type, data=end_event_data, duration=random.randint(1, 5))
    add_event_to_fel(start_event)
    add_event_to_fel(end_event)


async def events_handler(socket):
    generate_ship_travel()
    generate_plane_travel()
    while True:
        event = fel.pop(0)
        if event.generate_event:
            event.generate_event()
        await socket.send(json.dumps(event.data))
        await asyncio.sleep(parameters['time_speed'] * event.duration)


async def dna_handler(socket):
    while True:
        state['DNA'] += 1
        await socket.send(json.dumps({'type': 'DNA_INCREMENT', 'DNA': state['DNA']}))
        await asyncio.sleep(parameters['time_speed'] * 2)


async def population_handler(socket):
    while True:
        result = {}
        for continent in state['continents']:
            continent.apply_plague_impact()
            result[continent.name] = {
                'infected': continent.infected,
                'deaths': continent.deaths,
                'healthy': continent.healthy,
                'population': continent.population}
        await socket.send(
            json.dumps({'type': 'CONTINENT_INFECTED', 'continents': result}))
        await asyncio.sleep(parameters['time_speed'])


async def server(websocket, path):
    print('start')
    loop = asyncio.get_event_loop()
    loop.create_task(dna_handler(websocket))
    loop.create_task(population_handler(websocket))
    loop.create_task(events_handler(websocket))
    async for message in websocket:
        data = json.loads(message)
        if data["type"] == "START_GAME":
            state['plague'] = Plague(data['plague_name'])
            try:
                continent = [_ for _ in state['continents'] if _.name == data['start_continent']][0]
                continent.infected = 1
            except IndexError:
                print('ERROR CONTINENT WITH NAME {name} NOT FOUND'.format(name=data['start_continent']))
        elif data["type"] == "BUY_TRANSMISSION":
            print("BUY_TRANSMISSION")
        else:
            logging.error("unsupported event: {}", data)


generate_ship_travel = functools.partial(generate_travel, 'SHIP_TRAVEL', 'ship')
generate_plane_travel = functools.partial(generate_travel, 'PLANE_TRAVEL', 'plane')
load_continents()
load_travels()
start_server = websockets.serve(server, "localhost", 1337)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
