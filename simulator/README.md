This is the backend created using python 3

To run this project follow these steps:

1. Install python 3
1. Move to the simulator folder
    ```bash
    cd simulator
    ```
1. Create a virtual environment
    ```bash
    python -m venv venv
    ```
    Note: A virtual environment is a folder where you can add the packages per project instead of installing packages global, is the equivalent of the node_modules folder.
    
    The previous command will create a virtual environment folder where it will store all the libraries in a folder called "venv" (Notice the last keyword in the command)
1. Activate the virtual environment
    This step depends on the operative system
    
    For unix based systems:
    ```bash
    source venv/bin/activate
    ```
    For windows:
    
    Note: I haven't test this step on windows so feel free to update this readme.
    In theory it would be enough if you execute:
    ```bash
    ./venv/Scripts/activate.bat
    ```
    See this [guide](https://www.pythonmembers.club/2018/08/10/venv-usage-on-windows-activate-and-deactivate/) if you have trouble with this step.
    
    The idea is that you execute the activate script created in the virtual environment folder somehow
1. Install the dependencies
    ```bash
    pip install -r requirements.txt
    ```
1. Run the project
    ```bash
    python simulator.py
    ```

## Possible issues
1. Make sure you are using python 3
Test the python version with
```bash
python --version
```
The response should be like:
```bash
Python 3.7.3
```
It's common to have multiple versions of python installed, so it's usual that the 3 version is called "python3" instead of "python".
