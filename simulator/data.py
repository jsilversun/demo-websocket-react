continents = [
    {
        "name": "South America",
        "population": 100,
        "is_wealthy": False,
        "is_urban": False
    },
    {
        "name": "Central America",
        "population": 410000,
        "is_wealthy": True,
        "is_urban": True
    },
    {
        "name": "Northern America",
        "population": 410000,
        "is_wealthy": True,
        "is_urban": True
    },
    {
        "name": "Asia",
        "population": 410000,
        "is_wealthy": True,
        "is_urban": True
    }
]
travels = [
    {
        'continent1': 'South America',
        'continent2': 'Central America',
        'frequency': 1,
        'type': 'plane'
    },
    {
        'continent1': 'Central America',
        'continent2': 'Northern America',
        'frequency': 1,
        'type': 'ship'
    },
    {
        'continent1': 'Central America',
        'continent2': 'Asia',
        'frequency': 1,
        'type': 'ship'
    }
]
