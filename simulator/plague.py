class Plague:
    severity = 0
    lethality = 0
    infectivity = 0
    name = 'Plague'

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return '{name}'.format(name=self.name)
