class Travel:
    continent1 = None
    continent2 = None
    active = True
    frequency = 0
    type = None

    def __init__(self, type, continent1, continent2, frequency):
        self.type = type
        self.continent1 = continent1
        self.continent2 = continent2
        self.frequency = frequency

    def __str__(self):
        return '{type} - {continent1} - {continent2}'.format(
            type=self.type, continent1=self.continent1.name, continent2=self.continent2.name)
